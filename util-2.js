/**
 * --
 * @author solodoff@yandex.ru
 * upd 29.05.2014
 * --
 *
 */

'use strict';

var util = util || {};


...
...




/**
* Склоняет слово по числу.
*
* @example
* 1 -> год, 2 -> года, 10 -> лет
* decl(1, "год", "года", "лет") -> год
* decl(210, "рубль", "рубля", "рублей") -> рублей
*
* @param {number} number число
* @param {string} form1 форма слова: "Один ..." (Один год)
* @param {string} form2 форма слова: "Два ..." (Два года)
* @param {string} form3 форма слова: "Пять ..." (Пять лет)
* @return {string}
*/
util.decl(number, form1, form2, form3) {
  number = number % 100;

  if(Math.floor(number/10) == 1){
    return form3;
  }

  number = number % 10;

  return [form3, form1, form2, form2, form2, form3, form3, form3, form3, form3][number];
}


util.decl_roubles = function(number) {
  return util.decl(number, 'рубль', 'рубля', 'рублей');
};




/**
 * Заменяет спецсимволы HTML мнемониками для корректного отображения в браузере
 * @param {string} text текст
 * @return {string} текст с мнемониками
 */
util.escape_html = function(text) {
  return $('<div />').text(text).html();
};

/**
 * Форматирование чисел
 * Пример с пробелом 1234567.1234 > 1 234 567.1234
 *
 * @param {Number} num
 * @param {String|Number} delimiter строка или код символа
 * @return {String}
 */
util.format_num = function(num, delimiter) {
  delimiter = delimiter || 8201; //&thinsp

  if (typeof(delimiter) == 'number') {
    delimiter = String.fromCharCode(delimiter);
  }
  var parts = num.toString().split('.');
  return parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + delimiter) + (parts[1] ? '.' + parts[1] : '');
};

util.getURLParameter = function(name) {
  return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
};

util.getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

util.roundFloat = function(number, precision) {
  var x = Math.pow(10, precision);
  return Math.round(number * x) / x;
};

/**
 * Поиск в массиве объектов
 * @param {Array} arr
 * @param {String} prop имя свойства объекта, по котому идет поиск
 * @param {} s искомое значение
 * @return {Object|null}
 */
util.searchObj = function(arr, prop, s) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i][prop] == s) {
      return arr[i];
    }
  }
  return null;
};

/**
 * Перемешивает массив
 * @param {Array} array
 * @return {Array}
 */
util.arrayShuffle = function(array) {
  for (var i = array.length - 1; i > 0; i--) {
    var num = Math.floor(Math.random() * (i + 1));
    var d = array[num];
    array[num] = array[i];
    array[i] = d;
  }
  return array;
};

util.proportion = function(a1, a2, a, b1, b2, noLimits) {
  var p = (a - a1) / (a2 - a1);
  if(!noLimits){
    if (p < 0) {
      p = 0;
    }
    if (p > 1) {
      p = 1;
    }
  }
  return b1 + (b2 - b1) * p;
};

/**
 * Редирект
 *
 * @todo: переписать на history API
 *
 * @param uri
 */
util.redirect = function(uri) {
  window.location = uri;
};


/**
 * Плавно скроллит страницу до указанной позиции или до указанного элемента
 *
 * ToDo ?  $('html,body').animate({scrollTop: xxxx}, 500);
 *
 * @param {number | jQuery element} to
 * @param {object} animOptions
 */
util.scrollTo = function(to, animOptions) {
  var $window = $(window);
  var position = typeof(to) == 'number' ? to : to.offset().top;

  var options = $.extend({}, animOptions, {
    'step': function(now) {
      window.scrollTo(0, Math.round(now));
    }
  });

  $('<div/>')
    .css('top', $window.scrollTop() + 'px')
    .animate({
      'top': position + 'px'
    }, options);
};
