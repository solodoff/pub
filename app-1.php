<?php
/*...
 *
 */

class app {

    // логи
    const LOG_INFO = 1;
    const LOG_DEBUG = 2;
    const LOG_ERROR = 3;
    const LOG_WARNING = 4;

    // типы слотов
    const SLOT_PHOTO = 1;
    const SLOT_COMMENT = 2;

    // типы форматов фото
    const PHOTO_JPG = 1;
    const PHOTO_PNG = 2;

    // стили альбома
    const ALBUM_STYLE_DEFAULT = 1;
    const ALBUM_STYLE_GRAYSCALE = 2;
    const ALBUM_STYLE_LOMO = 3;

    ...


    // статусы заказа на печать
    /** @var int Помещён на конвеер */
    const PRINT_STATUS_START = 1;

    /** @var int Запущена генерация рендера */
    const PRINT_STATUS_RENDER = 2;

    /** @var int Готов к отправке подрядчику */
    const PRINT_STATUS_RENDER_STOP = 3;

    /** @var int Отправлен подрядчику */
    const PRINT_STATUS_PRINTING = 4;

    /** @var int Напечатан */
    const PRINT_STATUS_PRINTING_STOP = 5;

    /** @var int Отправлен пользователю */
    const PRINT_STATUS_DELIVERY = 6;

    ...
    //


    // НАВИГАЦИЯ

    // страницы (главная / страницы автора с его альбомами / страница альбома)
    const NAV_PAGE_MAIN = 1;
    const NAV_PAGE_USER = 2;
    const NAV_PAGE_ALBUM = 3;

    // аутентификация
    ...
    ...


    // принадлежность страницы (моя / не моя)
    const NAV_OWNER_USERPAGE_MINE = 1;
    const NAV_OWNER_USERPAGE_SOMEBODY = 2;

    // принадлежность альбома (общий / мой / чужой)
    const NAV_OWNER_ALBUM_COMMON = 1;
    const NAV_OWNER_ALBUM_MINE = 2;
    const NAV_OWNER_ALBUM_SOMEBODY = 3;


    // [ Singleton ]

    private static $instances = array();

    /**
    * Возвращает единственный экземпляр класса
    * @param string $path Абсолютный путь к классу
    * @param string $className Имя класса, оно же имя файла
    * @return object|null Экземпляр класса
    */
    private static function get($path, $className)
    {
        if (isset(self::$instances[$className])) return self::$instances[$className];
	$file = $path.SEP.$className.'.php';
        if(file_exists($file)) include_once $file; else return null;
	return self::$instances[$className] = new $className;
    }

    /**
    * Удаляет экземпляр класса
    * @param string $className Имя класса
    * @return void
    */
    private static function kill($className)
    {
	if (isset(self::$instances[$className])) unset(self::$instances[$className]);
    }

    // [ Singleton end ]

    // []

    /** @return core */
    public static function core() { return self::get(CFG_PATH_CLASSES, __FUNCTION__); }

    /** @return _libConnect */
    public static function _libConnect() { return self::get(CFG_PATH_CLASSES, __FUNCTION__); }

    /** @return router */
    public static function router() { return self::get(CFG_PATH_CLASSES, __FUNCTION__); }

    /** @return MySQL */
    public static function MySQL() { return self::get(CFG_PATH_CLASSES, __FUNCTION__); }

    ...

    ...


    /**
     * Logging
     * @param string $str log text
     * @param int $type type of log, constant app::LOG_...)
     * @return void
     */
    public static function log($str, $type = null) { return self::logging()->log($str, $type); }

    /** @return logging */
    private static function logging() { return self::get(CFG_PATH_CLASSES, __FUNCTION__); }




    // [ controllers ]

    /**
     * Получает объект контроллера
     * @param string $name Имя контроллера
     * @return object|null
     */
    public static function getController($name) { return self::get(CFG_PATH_CONTROLLERS, 'controller_'.$name); }


    // [ views ]

    /** @return view_main */
    public static function view_main() { return self::get(CFG_PATH_VIEWS, __FUNCTION__); }

    /** @return view_album */
    public static function view_album() { return self::get(CFG_PATH_VIEWS, __FUNCTION__); }

    /** @return view_author */
    public static function view_author() { return self::get(CFG_PATH_VIEWS, __FUNCTION__); }

    ...

    ...


    /** @return view_test */
    public static function view_test() { return self::get(CFG_PATH_VIEWS, __FUNCTION__); }



    // [ models ]

    /** @return manager_file */
    public static function manager_file() { return self::get(CFG_PATH_MODELS, __FUNCTION__); }

    /** @return manager_storage */
    public static function manager_storage() { return self::get(CFG_PATH_MODELS, __FUNCTION__); }

    /** @return manager_DB */
    public static function manager_DB() { return self::get(CFG_PATH_MODELS, __FUNCTION__); }

    /** @return manager_sms */
    public static function manager_sms() { return self::get(CFG_PATH_MODELS, __FUNCTION__); }

    ...

    ...



    /** @return manager_stat */
    public static function manager_stat() { return self::get(CFG_PATH_MODELS, __FUNCTION__); }

    /** @return manager_print */
    public static function manager_print() { return self::get(CFG_PATH_MODELS, __FUNCTION__); }



    // [ настройки ]

    ...
    ...


    /** @return string|void */
    public static function cfg_TEXT_SMS_PRINT_PRINTED($value=null) {
        if($value===null) return (string)self::manager_DB()->getCfgParam(substr(__FUNCTION__, 4));
        else return self::manager_DB()->setCfgParam(substr(__FUNCTION__, 4), $value);
    }



}
?>
