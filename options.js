import ACTION from '../actions/TYPES';


const initialState = {
  isShortView: false,
  filter: ''
};



export default function options(state = initialState, action) {
  switch(action.type){
    case ACTION.CHANGE_VIEW_MODE:
      return {
        ...state,
        isShortView: !state.isShortView
      };

    case ACTION.FILTER_TASKS:
      return {
        ...state,
        filter: action.payload
      };

    default:
      return state;
  }
}