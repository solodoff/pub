/**
 *  @author solodoff@yandex.ru
 *
 *  Зависимости:
 *    - jquery
 *    - ScrollObserver(solodoff@yandex.ru)
 *
 *  ToDo:
 *
 *
 *
 *  Следит за якорными ссылками при прокрутке и ресайзах.
 *  Триггерит событие на элементе <body>, куда документ прокручивается до следующей ссылки.
 *
 *  Использование:
 *    $('body').on('anchorLink', function(e, anchorElem){
 *      // ...
 *    });
 *
 *    var anchorLinksObserver = new AnchorLinksObserver( $anchors, options );
 *      где $anchors - jQuery array
 *      где options:
 *      {
 *        correction: сколько пикселей над ссылкой она считается активной
 *      }
 *
 *  События на элементе body:
 *    anchorLink вторым аргументом передаётся DOM-элемент активного якоря
 *
 */

import $ from 'jquery';
import ScrollObserver from 'module.ScrollObserver';  //  ToDo: в конфиге нужно прописать алиасы, чтобы так писать

function AnchorLinksObserver($anchors, options){
  this.options = $.extend({}, AnchorLinksObserver.prototype.DEFAULTS, options);
  this.$anchors = $anchors;
  this.$body = $('body');
  this.anchors = [];
  this.currentIndex = -1;

  this._init();
}

AnchorLinksObserver.prototype.DEFAULTS = {
  correction: 1
};

AnchorLinksObserver.prototype._init = function(){
  var t = this;


  this.$anchors.each(function(){
    t.anchors.push({
      elem: this,
      offsetTop: 0
    });
  });

  this.$body.on('go', function(e, res){
    var type = res.event.type;
    var scrollTop = res.window.scroll.top;

    if(type != 'scroll' && type != 'touchmove'){
      t._refreshOffsets();
    }

    // проверяем какой якорь в поле зрения и триггерим соотв. событие
    t._check(scrollTop);
  });
};

AnchorLinksObserver.prototype._refreshOffsets = function(){
  for(var i=0; i<this.anchors.length; i++){
    this.anchors[i].offsetTop = $(this.anchors[i].elem).offset().top;
  }
};

AnchorLinksObserver.prototype._check = function(scrollTop){
  for(var j=this.anchors.length-1; j>=0; j--){
    if(this.anchors[j].offsetTop - this.options.correction <= scrollTop){
      if(j !== this.currentIndex){
        this.currentIndex = j;
        this._trigger(this.anchors[j].elem);
      }
      break;
    }
  }
};

AnchorLinksObserver.prototype._trigger = function(anchorElem){
  this.$body.trigger('anchorLink', [anchorElem]);
};

export default AnchorLinksObserver;
