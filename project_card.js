$(function(){

  $('.project_tabs').tabset();
  $('.itemset').itemset();

  $('input.data_fields_company').autocomplete({
    source: PROJECT_DATA.companies,
    minLength: 1,
    select: function(event, ui){
      //ui.item.
      //this.value

      var f = projectCardVM.data.fields;
      f.company_id(ui.item.id);
      f.company_name(ui.item.value);
    }
  });

  ...
  ...



  /* datepicker */

  var dateFormat = 'dd.mm.yy';
  $.datepicker.setDefaults($.datepicker.regional['ru']);
  $.datepicker.setDefaults({
    showAnim: 'fadeIn',
    showOtherMonths: true,
    selectOtherMonths: true,
    showButtonPanel: true,
    changeMonth: true,
    changeYear: true,
    dateFormat: dateFormat
  });
  $('.datepicker').datepicker();
  $(document).on('blur', '.datepicker', function(){
      if(!isValidDate(this.value)){
        this.value = '';
      }
  });

  var isValidDate = function(date){
    try{
      $.datepicker.parseDate(dateFormat, date);
    }
    catch(exception){
      return false;
    }
    return true;
  };


  ...


  var Stage = function(id, days, date, cost, hours_estimate, hours_plan, hours_fact){
    this.id = id;
    this.days = days || '';
    this.date = ko.observable(date || '');

    ...

    this.cost = ko.observable(cost);
    this.hours_estimate = ko.observable(hours_estimate);
    this.hours_plan = ko.observable(hours_plan || 0);
    this.hours_fact = ko.observable(hours_fact || 0);
  };

  var initObj = function(data){
    var f = data.fields;
    f.company_projects = ko.observable(f.company_projects);
    f.cost_value = ko.observable(f.cost_value);
    f.seller_id = ko.observable(f.seller_id);
    f.manager_id = ko.observable(f.manager_id);
    ...
    ...

    var stages = $.map(f.stages, function(stage){
		var s = new Stage(
        stage.id,
        stage.days,
        stage.date,
        stage.cost,
        stage.hours_estimate,
        stage.hours_plan,
        stage.hours_fact
      );

	  s.date.subscribe(function(val){
		console.log('TEST 3: ', val, this);

	  });
      return s;
    });
    f.stages = ko.observableArray(stages);

  };


  /* projectCard View-Model */

  var VM = function(data){
    var t = this;
    this.data = data;
    var d = data;
    var f = data.fields;
    initObj(d);

    this.hashLink = ko.observable("");

    this.isValid = function(value, type, min, max, pattern){
       if(pattern){
         var s=value ? value.toString() : '';
         if(s.search(new RegExp(pattern)) < 0) {
           return false;
         }
      }
      if(value == undefined || value == '')
        return true;

      if(type == 'short_date'){
        try{
          value_date = $.datepicker.parseDate(dateFormat, value);
          ...

          return true; // date value within the specified range
        } catch(exception) {
          return false; // could not parse value as a date
        }
      } else if(type == 'int'){
        return ((min == undefined || value >= min) && (max == undefined || value <= max));
      }
      return true;
    };



    this.applyPlugins = function(node){
      $(node).find('.datepicker').datepicker()
      .end().find('.data_fields_stages_cost').numberMask({pattern: pattern_int})
      .end().find('.data_fields_stages_days, .data_fields_stages_hours_estimate').numberMask({pattern: pattern_int});
    };

    this.sortableHelper = function(e, ui){
      return ui.clone().children().hide().first().show();
    };


    this.refreshProjectTypes = ko.computed(function(){
      var department = app.searchObj(d.departments, 'id', f.department_id());
      if(!department)
        return '';

      f.project_types($.map(department.types, function(v){
        return app.searchObj(d.types, 'id', v);
      }));
    });

    this.refreshStages = ko.computed(function(){
      var id = f.project_type_id();

      if(f.stages_init_flag){
        f.stages_init_flag = false;
        return;
      }

      var type = app.searchObj(d.types, 'id', id);
      if(!type) return [];

      f.stages($.map(type.stages, function(v){
        return new Stage(v);
      }));
    });

    this.getStageName = function(id){
      var stageName = app.searchObj(d.stages, 'id', id);
      return stageName ? stageName.name : '';
    };

    this.addStage = function(){
      f.stages.push(new Stage(f.stage_id_new()));
    };

    this.removeStage = function(stage, event){
      var $row = $(event.target).closest('tr');
      $row.animate({'opacity': 0}, 360, function(){
        f.stages.remove(stage);
      });
    };

    this.getProjectType = ko.computed(function(){
      var type = app.searchObj(d.types, 'id', f.project_type_id());
      return type ? type.name : '';
    });

    this.getDepartment = ko.computed(function(){
      var department = app.searchObj(d.departments, 'id', f.department_id());
      return department ? department.name : '';
    });

    this.getManager = ko.computed(function(){
      var manager = app.searchObj(d.managers, 'id', f.manager_id());
      return manager ? manager.name : '';
    });

    this.getAccountManager = ko.computed(function(){
      var accountManager = app.searchObj(d.managers, 'id', f.account_manager_id());
      return accountManager ? accountManager.name : '';
    });

    this.getSeller = ko.computed(function(){
      var seller = app.searchObj(d.managers, 'id', f.seller_id());
      return seller ? seller.name : '';
    });

    this.getPriority = ko.computed(function(){
      var priority = app.searchObj(d.priorities, 'id', f.priority());
      return priority ? priority.name : '';
    });

    this.totalCost = ko.computed(function(){
      var res = 0;
      $.each(f.stages(), function(k, v){
        if(v.cost()) res += app.priceToNumber(v.cost());
      });
      return app.priceToString(res);
    });

    this.getTotalHoursEstimate = ko.computed(function(){
      var res = 0;
      $.each(f.stages(), function(k, v){
        if(v.hours_estimate()) res += app.toInt(v.hours_estimate());
      });
      return res;
    });

    this.getTotalHours = ko.computed(function(){
      var res1 = 0;
      var res2 = 0;
      $.each(f.stages(), function(k, v){
        res1 += app.toInt(v.hours_plan());
        res2 += app.toInt(v.hours_fact());
      });
      return res1+' / '+res2;
    });

    this.upStage = function(stage, event){
      var index = f.stages.indexOf(stage);
      if(index == 0) return;

      var $curr = $(event.target).closest('tr');
      var $prev = $curr.prev();
      $prev.animate({'opacity': 0}, 260, function(){
        $curr.animate({'opacity': 0}, 260, function(){
          f.stages.splice(index-1,0,f.stages.remove(stage)[0]);
          $prev.delay(250).animate({'opacity': 1});
        });
      });
    };

    ...

    this.downStage = function(stage, event){
      var index = f.stages.indexOf(stage);
      if(index == f.stages().length-1) return;

      var $curr = $(event.target).closest('tr');
      var $next = $curr.next();
      $next.animate({'opacity': 0}, 260, function(){
        $curr.animate({'opacity': 0}, 260, function(){
          f.stages.splice(index+1,0,f.stages.remove(stage)[0]);
          $next.delay(250).animate({'opacity': 1});
        });
      });
    };


    /**
     * @return {Array} Список доступных этапов для текущего типа проекта
     */
    this.availableStages = ko.computed(function(){
      var types = f.project_types();
      if(!types.length) return [];

      var type = app.searchObj(types, 'id', f.project_type_id());
      var availableStagesIDs = type ? type.stages : [];

      return $.map(t.data.stages, function(elem, index){
        if(!availableStagesIDs.length || (app.searchInArray(availableStagesIDs, elem.id) != -1)){
          return elem;
        }
      });
    });

    /**
     * @return {Array} Список этапов, которые ещё не задействованы
     */
    this.remainingStages = ko.computed(function(){
      var addedStagesIDs = [];
      $.each(f.stages(), function(index, value){
        addedStagesIDs.push(value.id);
      });

      return $.map(t.availableStages(), function(elem, index){
        if(app.searchInArray(addedStagesIDs, elem.id) == -1){
          return elem;
        }
      });
    });


    this.checkCost = ko.computed(function(){
      if(!f.project_types().length) return true;
      return f.cost_value() == t.totalCost();
    });


    /**
     * Результат работ
     */
    this.setResultType = function(data, event){
      var $target = $(event.target);
      if($target.hasClass("link")){
        data.fields.result_type($target.text());
      }
    }
  };

  var projectCardVM = new VM(PROJECT_DATA);
  ko.applyBindings(projectCardVM);

  /**/

  projectCardVM.checkCost.subscribe(function(val){
    hint(!val);
  });


  /* Активный таб связан с урлом */
  $(".project_tabs .itemset_item").filter("[data-tabset=" + app.locationHash() + "]").click();

  $(".project_tabs").on("changeState.itemset", function(){
    var newHash = $(this).find(".itemset_item.selected").attr("data-tabset");
    app.locationHash(newHash);

    projectCardVM.hashLink(newHash);
  }).trigger("changeState.itemset");

});
