<?php
/**

 */
class core {

    /**
     * Редирект
     * @param $location абсолютный адрес
     * @return void
     */
    public function redirect($location=CFG_SITE_ROOT) {
        header('Location: '.$location);
        exit;
    }

    /**
     * Удаляем кукисы и сессию
     */
    public function session_destroy(){
        $_SESSION = array();
        unset($_COOKIE[session_name()]);
        session_destroy();
    }

    /**
     * Немедленно завершает скрипт с выбросом сообщения в лог
     * @return void
     */
    public function exitApp($log_message){
        app::log($log_message, app::LOG_ERROR);
        exit;
    }

    public function charsetToWIN1251($s){
        return iconv('UTF-8', 'windows-1251', $s);
    }

    public function charsetToUTF8($s){
        return iconv('windows-1251', 'UTF-8', $s);
    }

    /**
     * Возвращает данные, необходимые для фронтенда
     * @param int $page app::NAV_PAGE_...
     * @param int $user_id_current
     * @param int $album_id_current
     * @return array
     */
    public function getData($page, $user_id_current=null, $album_id_current=null){
        $data = array();
        ...
        ...

        return $data;
    }

    /**
     * Проверяет, является ли строка номером телефона (по маске +7 000 000-00-00)
     * @param string $str
     * @return bool
     */
    public function is_phone($str){
        return (bool)preg_match('/^\+7 \d{3} \d{3}-\d{2}-\d{2}$/is', $str);
    }

    /**
     * Проверяет, является ли строка пин-кодом (по маске 0000)
     * @param string $str
     * @return bool
     */
    public function is_pin($str){
        return (bool)preg_match('/^\d{4}$/is', $str);
    }

    ...
    ...

    /**
     * Запускает рендеринг отдельным процессом
     * @param int $print_id
     * @return void
     */
    public function forkRender($print_id){
        $path = CFG_PATH_ROOT.'/www/index.php';
        $args = $print_id;

        $command = 'nohup php '.$path.' '.$args.' > /dev/null 2>&1 & echo $!';
        app::log("команда: $command", app::LOG_DEBUG);

        exec($command);
    }

    ...
    ...


}
?>
