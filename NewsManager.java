package com.wg.core;

import com.wg.db.entity.NewsEntity;
import com.wg.db.entity.NewsSourcesEntity;
import com.wg.db.entity.NewsPrivilegedAlliancesEntity;
import com.wg.db.entity.AllianceEntity;

import javax.persistence.Query;
import java.util.*;

public class NewsManager extends AbstractManager
{
    public NewsManager()
    {
        this.Init_number_of_news();
    }

    ...

    ...

    /**
     * возвращает страницу новостей из результата запроса
     */
    private List get_page(Query q, int news_per_page, int num)
    {
        int start = num * news_per_page - news_per_page;
        int end = num * news_per_page - 1;
        if( q.getResultList().size() < (end + 1) ) end = q.getResultList().size()-1;

        return q.getResultList().subList(start, end+1);
    }

    /**
     * возвращает имя источника по его ID
     */
    private String get_source_name(int source_id)
    {
        Query q = getEm().createQuery("FROM NewsSourcesEntity WHERE ID = '"+source_id+"'");
        if( q != null && q.getResultList().size() != 0)
        {
            NewsSourcesEntity res = (NewsSourcesEntity)q.getSingleResult();
            return res.getSOURCE();
        }
        else return null;
    }

    /**
     * возвращает страницу новостей
     */
    public List News_show_news_page(int num, int news_per_page, int source_id)
    {
        List res = null;

        String query = "";
        if( source_id == 0 ) query = "FROM NewsEntity WHERE IS_PUBLISHED = true ORDER BY DATE DESC";
        else
        {
            String source_name = this.get_source_name(source_id);
            if(source_name != null) query = "FROM NewsEntity WHERE SOURCE = '"+source_name+"' AND IS_PUBLISHED = true ORDER BY DATE DESC";
            else return null;
        }
        Query q = getEm().createQuery(query);
        if( q != null && q.getResultList().size() != 0 ) res = this.get_page(q, news_per_page, num);

        return res;
    }


    /**
     * возвращает -num- последних новостей
     */
    public List News_Get_Last_News(int num)
    {
        List res = null;
        Query q = getEm().createQuery("FROM NewsEntity WHERE IS_PUBLISHED = true ORDER BY DATE DESC");

        if( q != null && q.getResultList().size() != 0 )
        {
            if( q.getResultList().size() >= num ) res = q.getResultList().subList(0, num);
            else res = q.getResultList().subList(0, q.getResultList().size());
        }

        return res;
    }

    /**
     * добавляет новость
     */
    public boolean News_add(String source, String title, String annotation, String news, boolean is_published)
    {
        NewsEntity new_news = new NewsEntity();

        new_news.setSOURCE(source);
        new_news.setTITLE(title);
        new_news.setANNOTATION(annotation);
        new_news.setNEWS(news);
        new_news.setDATE(new Date());
        new_news.setIS_PUBLISHED(is_published);

        try { createObject(new_news); } catch (Exception e) { e.printStackTrace(); return false; }
        this.Init_number_of_news();
        return true;

    }

    /**
     * удаляет новость
     */
    public boolean News_delete(int id, boolean is_published)
    {
        NewsEntity news = getNewsById(id, is_published);

        try { removeObject(news); } catch (Exception e) { e.printStackTrace(); return false; }
        this.Init_number_of_news();
        return true;
    }

    /**
     * изменяет новость
     */
    public boolean News_update(int id, String source, boolean is_refresh_date, String title, String annotation, String news, boolean is_published)
    {
        NewsEntity current_news = this.getNewsById(id, is_published);
        current_news.setSOURCE(source);
        if(is_refresh_date)current_news.setDATE(new Date());
        current_news.setTITLE(title);
        current_news.setANNOTATION(annotation);
        current_news.setNEWS(news);

        try { updateObject(current_news); } catch (Exception e) { e.printStackTrace(); return false; }
        return true;
    }

    /**
     * возвращает новость по её ID
     */
    public NewsEntity getNewsById(int id, boolean is_published)
    {
        String tmp = "true";
        if(!is_published) tmp = "false";
        Query q = getEm().createQuery("FROM NewsEntity WHERE ID="+id+"AND IS_PUBLISHED = "+tmp);
        if( q != null && q.getResultList().size() != 0 ) return (NewsEntity)q.getSingleResult();
        else return null;
    }

    /**
     * добавляет источник
     */
    public boolean News_Source_add(String source)
    {
        NewsSourcesEntity newsource = new NewsSourcesEntity();
        newsource.setSOURCE(source);

        try { createObject(newsource); } catch (Exception e) { e.printStackTrace(); return false; }
        return true;
    }

    /**
     * возвращает источники
     */
    public List News_Get_Sources()
    {

        List res = null;
        Query q = getEm().createQuery("FROM NewsSourcesEntity ORDER BY ID");
        if( q != null && q.getResultList().size() != 0 ) res = q.getResultList();

        return res;
    }

    /**
     * публикует новость
     */
    public boolean News_publish(int id)
    {
        NewsEntity current_news = this.getNewsById(id, false);
        current_news.setIS_PUBLISHED(true);
        current_news.setDATE(new Date());

        try { updateObject(current_news); } catch (Exception e) { e.printStackTrace(); return false; }
        this.Init_number_of_news();
        return true;
    }

    /**
     * возвращает список всех альянсов
     */
    public List News_Get_Alliances()
    {
        List res = null;
        Query q = getEm().createQuery("FROM AllianceEntity ORDER BY ID");
        if( q != null && q.getResultList().size() != 0 ) res = q.getResultList();

        return res;
    }

    /**
     * возвращает список привилегированных альянсов
     */
    public List News_Get_Privileged_Alliances()
    {
        List res = null;
        Query q = getEm().createQuery("FROM NewsPrivilegedAlliancesEntity ORDER BY ID");
        if( q != null && q.getResultList().size() != 0 ) res = q.getResultList();

        return res;
    }

    /**
     * возвращает привилегированный альянс по его -NAME-
     */
    private NewsPrivilegedAlliancesEntity getPrivilegedAllianceByName(String name)
    {
        Query q = getEm().createQuery("FROM NewsPrivilegedAlliancesEntity WHERE ALLIANCE_NAME= '"+name+"'");
        if( q != null && q.getResultList().size() == 1 ) return (NewsPrivilegedAlliancesEntity)q.getSingleResult();
        else return null;
    }

    /**
     * удаляет альянс из привилегированных
     */
    public boolean News_delete_Privileged_Alliance(String name)
    {
        NewsPrivilegedAlliancesEntity privileged_alliance = getPrivilegedAllianceByName(name);
        if(privileged_alliance == null) return false;
        //
        try { removeObject(privileged_alliance); } catch (Exception e) { e.printStackTrace(); return false; }
        return true;
    }

    /**
     * добавляет альянс в привилегированные
     */
    public boolean News_add_Privileged_Alliance(String name)
    {
        NewsPrivilegedAlliancesEntity new_privileged_alliance = new NewsPrivilegedAlliancesEntity();
        new_privileged_alliance.setALLIANCE_NAME(name);
        //
        try { createObject(new_privileged_alliance); } catch (Exception e) { e.printStackTrace(); return false; }
        return true;
    }

    /**
     * возвращает неопубликованные новости альянсов
     */
    public List News_Get_Alliances_News()
    {
        List res = null;
        Query q = getEm().createQuery("FROM NewsEntity WHERE IS_PUBLISHED = false ORDER BY DATE");
        if( q != null ) res = q.getResultList();
        return res;
    }

    ...

    /**
     * проверяет, является ли пользователь лидером привилегированного альянса; возвращает название альянса
     */
    public String News_is_Leader_of_Privileged_Alliance(long id)
    {
        String res = null;
        Query q = getEm().createQuery("FROM AllianceEntity WHERE LEADER = "+id);

        if( q != null && q.getResultList().size() == 1 )
        {
            AllianceEntity alliance = (AllianceEntity)q.getSingleResult();
            String alliance_name = alliance.getNAME();
            q = getEm().createQuery("FROM NewsPrivilegedAlliancesEntity WHERE ALLIANCE_NAME = '"+alliance_name+"'");
            if( q != null && q.getResultList().size() == 1 ) res = alliance_name;
        }

        return res;
    }

    ...

    /**
     * Проверяет, является ли -source- источником
     */
    public boolean News_is_Source(String source)
    {
        Query q = getEm().createQuery("FROM NewsSourcesEntity WHERE SOURCE = '"+source+"'");
        if( q != null && q.getResultList().size() > 0 ) return true;
        else return false;
    }

}
