/**
 *  @author solodoff@yandex.ru
 *  upd 30.07.2013
 *  --
 *
 *  IE9+
 *
 *  Зависимости:
 *    jquery
 *    jquery.easing -необязательно, для нестандартных изингов
 *
 *  Виджет позволяет сравнить содержимое блоков.
 *  Содержимым м.б. как картинка, так и любой HTML.
 *  Линия-разделитель находится всегда под курсором.
 *  Включён функционал плавной смены изображений.
 *
 *
 *  Использование:
 *    var widget1 = new BeforeAndAfter(container, {options}, data);
 *
 *  CSS:
 *    Пример стилей разделителя:
 *    .after_wrapper{
 *      border-left: ...;
 *      box-shadow: ...;
 *    }
 *
 *  События на элементе .container:
 *    changepos.beforeAndAfter передаётся position(центральная точка)
 *
 *
 */
function BeforeAndAfter(container, options){
  this.options = $.extend({}, BeforeAndAfter.prototype.DEFAULTS, options);

  this.$container = $(container);
  this.$before = undefined;
  this.$after_wrapper = undefined;
  this.$after = undefined;

  this.angle_rad = this.options.angle * Math.PI / 180;
  this.delta = Math.abs(Math.round(this.options.height / 2 * Math.tan(this.angle_rad)));
  this.scrollLength = this.options.width + 2*this.delta;

  var pd = this.options.positionDefault;
  if(pd == 'start'){
    this.options.positionDefault = 100 * (2 * this.delta) / this.scrollLength;
  }else if(pd == 'end'){
    this.options.positionDefault = 100 * (this.options.width) / this.scrollLength;
  }else if(typeof(pd) != 'number'){
    this.options.positionDefault = 50;
  }

  this.timer_revert = undefined;
  this.counter = -1;

  this.buildStructure();

  if(this.options.isBindEvents){
    this.bindEvents();
  }

  if(this.options.isInit){
    this.init();
  }
}

BeforeAndAfter.prototype.DEFAULTS = {
  'dataIMG': undefined, // [['before1.jpg', 'after1.jpg'], ['before2.jpg', 'after2.jpg']]
  'dataHTML': undefined, // {'before': ..., 'after': ...}    String|jQuery|DOM Element
  'width': 300,
  'height': 100,
  'angle': 6,
  'delayNext': 4000,
  'delayBeforeRevert': 300,
  'durationAnim': 1000,
  'durationRevert': 580,
  'durationCursor': 100,
  'positionDefault': 'start', // 'start' | 'end' | 0-100(%)
  'isBindEvents': true,
  'isInit': true,
  'easingRevert': 'easeInOutBack',
  'easingCursor': 'easeOutCubic'//'linear'
};

BeforeAndAfter.prototype.buildStructure = function(){
  var skew = 'skewX(' + this.options.angle + 'deg)';
  var skew_rev = 'skewX(' + (-this.options.angle) + 'deg)';

  this.$container.css({
    'position': 'relative',
    'width': this.options.width+'px',
    'height': this.options.height+'px',
    'overflow': 'hidden'
  });

  this.$before = $('<div />').addClass('before').css({
    'position': 'relative',
    'width': this.options.width+'px',
    'height': this.options.height+'px'
  });

  this.$after_wrapper = $('<div />').addClass('after_wrapper').css({
    'position': 'absolute',
    'left': -this.delta+'px', // init
    'right': -this.delta+'px',
    'top': 0,
    'bottom': 0,
    'z-index': 1,
    'overflow': 'hidden',

    '-moz-transform': skew,
    '-ms-transform': skew,
    '-webkit-transform': skew,
    '-o-transform': skew,
    'transform': skew
  });

  this.$after = $('<div />').addClass('after').css({
    'position': 'absolute',
    'right' : this.delta+'px',
    'width': this.options.width+'px',
    'height': this.options.height+'px',

    '-moz-transform': skew_rev,
    '-ms-transform': skew_rev,
    '-webkit-transform': skew_rev,
    '-o-transform': skew_rev,
    'transform': skew_rev
  });

  this.$after_wrapper.append(this.$after);
  this.$container.append(this.$before, this.$after_wrapper);
};

BeforeAndAfter.prototype.bindEvents = function(){
  var widget = this;

  this.$container
  .on('mousemove.BeforeAndAfter', function(e){
    clearTimeout(widget.timer_revert);
    var offset = $(this).offset();
    var left = e.pageX-offset.left;
    var top = e.pageY-offset.top;
    widget.scrollCursor(left, top);
  })
  .on('mouseleave.BeforeAndAfter', function(){
    clearTimeout(widget.timer_revert);
    widget.timer_revert = setTimeout(function(){
      widget.scrollPercent(widget.options.positionDefault, widget.options.durationRevert);
    }, widget.options.delayBeforeRevert);

  });
};

BeforeAndAfter.prototype.init = function(){
  var widget = this;

  if(this.options.dataIMG){
    this.next(function(){
      widget.scrollPercent(widget.options.positionDefault, widget.options.durationRevert);
    }, true);

    if(this.options.dataIMG.length > 1){
      setInterval(function(){
        widget.next();
      }, this.options.delayNext);
    }
  }else{
    this.showHTML();
  }

};

BeforeAndAfter.prototype.next = function(finish, fast){
  this.counter++;
  if(this.counter == this.options.dataIMG.length){
    this.counter = 0;
  }
  this.showIMG(finish, fast);
};

BeforeAndAfter.prototype.showIMG = function(finish, fast){
  var widget = this;

  var imgBefore = new Image();
  var imgAfter = new Image();

  var d1 = $.Deferred();
  var d2 = $.Deferred();

  imgBefore.onload = d1.resolve;
  imgAfter.onload = d2.resolve;

  var srcData = this.options.dataIMG[this.counter];

  $.when(d1, d2).done(function(){//смена кадра
    var $ins1 = $('<div />');
    var $ins2 = $('<div />');

    $ins1.add($ins2).css({
      'position': 'absolute',
      'width': '100%',
      'height': '100%',
      'opacity': 0
    });

    widget.$before.append($ins1);
    widget.$after.append($ins2);

    $ins1.css('background', 'url('+srcData[0]+')');
    $ins2.css('background', 'url('+srcData[1]+')');

    $ins1.add($ins2).animate({'opacity': 1}, (fast ? 0 : widget.options.durationAnim), function(){
      widget.$before.css('background', 'url('+srcData[0]+')');
      widget.$after.css('background', 'url('+srcData[1]+')');
      $ins1.add($ins2).remove();
      if(finish) finish();
    });
  });

  imgBefore.src = srcData[0];
  imgAfter.src = srcData[1];
};

BeforeAndAfter.prototype.showHTML = function(finish){
  this.$before.append(this.options.dataHTML.before);
  this.$after.append(this.options.dataHTML.after);
  if(finish) finish();
};

BeforeAndAfter.prototype.scrollTo = function(position, duration, complete){
  position = Math.round(position);

  var widget = this;
  var easing = widget.options.easingRevert;

  if(!duration){
    duration = widget.options.durationCursor;
    easing = widget.options.easingCursor;
  }

  if(duration){
    this.$after_wrapper.stop().animate(
      {
        'left': position+'px'
      },
      {
        'duration': duration,
        'easing': easing,
        'step': function(now){
          widget.$container.trigger('changepos.beforeAndAfter', [parseInt(now, 10)]);
        },
        'complete': function(){
          if(complete) complete();
        }
      }
    );
  }
};

BeforeAndAfter.prototype.scrollPercent = function(position, duration, complete){
  var length = this.scrollLength * position / 100;
  this.scrollTo(length - this.delta, duration, complete);
};

BeforeAndAfter.prototype.scrollCursor = function(left, top){
  var dy = top - (this.options.height / 2);
  var dx = Math.tan(this.angle_rad) * dy;
  this.scrollTo(left - dx);
};
