<?php
/**
...
 */
class router {
    private $controller;
    private $action;
    private $args;

    public function __construct(){
        $this->controller = 'index';
        $this->action = 'main1';
        $this->args = array();
    }

    /**
     *
     * @param array $data=array() Готовый маршрут и данные для контроллера. {'controller'=>string, 'action'=>string, 'args'=>array}
     */
    public function delegate($data=array()){
        if(empty($data)){
            $this->parseRoute();
        }
        else{
            $this->controller = $data['controller'];
            $this->action = $data['action'];
            $this->args = $data['args'];
        }

        // получаем объект контроллера
        $controller = app::getController($this->controller);

        // проверяем наличие экшна в контроллере
        if($controller === null || !is_callable(array($controller, $this->action))) app::core()->redirect();

        call_user_func(array($controller, $this->action), $this->args);
    }

    /**
     * Разбор запроса
     */
    private function parseRoute(){
        $parts = explode('/', trim($_SERVER['REQUEST_URI'], '/'));

        $this->controller = empty($parts[0]) ? 'index' : $parts[0];
        $this->action = isset($parts[1]) && !empty($parts[1]) ? $parts[1] : 'index';

        ...

        if(count($parts)>2){
            ...

            //Разбор аргументов
            ...
        }
    }
}
?>
