/**
 *  --
 *  @author solodoff@yandex.ru
 *  upd 29.05.2014
 *  --
 *  Зависимости:
 *    - jquery
 *    - class.ScrollObserver (by solodoff@yandex.ru)
 *    - util.js (by solodoff@yandex.ru)
 *
 *  Фиксирует элемент на странице во время прокрутки.
 *  Плагин работает в двух режимах:
 *    - window (по умолчанию) - элемент либо находится на совём месте, либо прилип к верхней кромке окна;
 *    - wrapper - положение элемента зависит от обёртки. У обёртки должна быть задана высота.
 *      Когда прокрутка достигнет конца обёртки, элемент прилипнет к её нижней кромке.
 *      Так же этот режим можно использовать для параллакс-эффектов, высота обёртки задаёт длину прокрутки,
 *      на которой элемент остаётся на экране.
 *
 *  В режиме "window": (если у обёртки не задана высота, плагин вычислит её и пропишет)
 *    - пока обёртка в зоне видимости, элемент остаётся inline;
 *    - как только обёртка уходит из зоны видимости, элемент становится fixed.
 *
 *  В режиме "wrapper": (у обёртки должен быть position: relative)
 *    - пока верхняя часть обёртки в зоне видимости, элемент остаётся у верхней границы обёртки;
 *    - как только обёртка начинает уходить за верхнюю кромку окна, элемент становится fixed;
 *    - когда нижняя кромка обёртки доходит до элемента, элемент прибивается к ней.
 *
 *  CSS:
 *    На элемент по мере прокрутки вешаются классы, обозначающие 3 состояния фиксации элемента.
 *
 *    Пример стилей для состояний фиксации:
 *
 *    .fixer-state-normal{  // элемент встроен в документ
 *      position: static;
 *      top: auto;
 *      bottom: auto;
 *      left: auto;
 *      right: auto;
 *    }
 *
 *    .fixer-state-fixed{  // элемент прибит верхней частью к верхней кромке окна
 *      position: fixed;
 *      top: 0;
 *      bottom: auto;
 *      left: auto;
 *      right: auto;
 *    }
 *
 *    .fixer-state-bottom{  // элемент прибит нижней частью к нижней кромке обёртки
 *      position: absolute;
 *      top: auto;
 *      bottom: 0;
 *      left: auto;
 *      right: auto;
 *    }
 *
 *  События на элементе:
 *    go.jqueryFixer - триггерится при смене состояния фиксации,
 *    а в режиме wrapper-fixed триггерится постоянно и передаёт прогресс прокрутки (0 - в начале обёртки, 100 - в конце)
 *
 *    $(elem).on('go.jqueryFixer', function(e, res){
 *      //...
 *    });
 *
 *    res - объект:
 *    {
 *      state: 0-normal | 1-fixed | 2-bottom
 *      progress: 0..100 //
 *    }
 *
 */
;(function ($, window, document, undefined){
  var PLUGIN_NAME = 'fixer';

  var defaults = {
    mode: 'window', //  window | wrapper
    position: 0 // Будет считать не относительно верхней кромки окна, а по указанному сдвигу, в px. Не забыть сдвиг в CSS!
  };

  var methods_types = {
    'off': 'set'
  };

  $.fn[PLUGIN_NAME] = function(arg){
    if(typeof arg == 'string'){
      var a = Array.prototype.slice.call(arguments, 1);
      var type = methods_types[arg];

      switch(type){
        case 'set':
          return this.each(function(){
            $(this).data(PLUGIN_NAME).methods[arg].apply(this, a);
          });

        case 'get':
          if(this.length > 1){
            console.warn("Вызов метода '" + arg + "' для нескольких DOM-элементов!"
              + " Отработает метод только для первого элемента из набора.");
          }
          return this.eq(0).data(PLUGIN_NAME).methods[arg].apply(this, a);

        default:
          console.warn("Вызов несуществующего метода '" + arg + "'!");
          return this;
      }
    }
    if(!(typeof arg == 'object' || typeof arg == 'undefined')) return;
    /**/

    var STATE_NORMAL = 0;
    var STATE_FIXED = 1;
    var STATE_BOTTOM = 2;

    var CLASS_NAMES = ['fixer-state-normal', 'fixer-state-fixed', 'fixer-state-bottom'];

    var setElemState = function($elem, state){
      $elem.removeClass(CLASS_NAMES.join(' ')).addClass(CLASS_NAMES[state]);
    };

    var removeClasses = function($elem){
      $elem.removeClass(CLASS_NAMES.join(' '));
    };

    var trigger = function($elem, state, progress){
      $elem.trigger('go.jqueryFixer', [{
        state: state,
        progress: typeof(progress) == 'undefined' ? null : progress
      }]);
    };



    return this.each(function(){
      var options = $.extend({}, defaults, arg);
      var $elem = $(this);
      /**/
      var $wrapper = $elem.parent();
      var handler, newState, progress;
      var state = '';

      // режим window
      if(options.mode == 'window'){
        $wrapper.css({
          height: $wrapper.height() + 'px'
        });

        handler = function(e, res){
          var wrapperOffset = $wrapper.offset();

          if(wrapperOffset.top - options.position > res.window.scroll.top){
            newState = STATE_NORMAL;
          }else{
            newState = STATE_FIXED;
          }

          if(newState !== state){
            state = newState;
            setElemState($elem, state);
            trigger($elem, state);
          }
        };
      }

      // режим wrapper
      if(options.mode == 'wrapper'){
        handler = function(e, res){
          var scrollTop = res.window.scroll.top;
          var elemHeight = $elem.height();
          var wrapperOffset = $wrapper.offset();
          var wrapperHeight = $wrapper.height();

          if(wrapperOffset.top - options.position > scrollTop){
            newState = STATE_NORMAL;
            progress = 0;
          }else if((scrollTop + elemHeight + options.position) >= (wrapperOffset.top + wrapperHeight)){
            newState = STATE_BOTTOM;
            progress = 100;
          }else{
            newState = STATE_FIXED;
            progress = util.proportion(wrapperHeight, elemHeight, wrapperOffset.top + wrapperHeight - scrollTop - options.position, 0, 100, false);
          }

          if(newState !== state){
            state = newState;
            setElemState($elem, state);
            trigger($elem, state, progress);
          }else if(state == STATE_FIXED){
            trigger($elem, state, progress);
          }
        };
      }

      // init
      $('body').on('go.scrollObserver', handler);  //  событие go на элементе body триггерится объектом scrollObserver

      // data
      var methods = {
        'off': function(data){
          $('body').off('go.scrollObserver', handler);
          removeClasses($elem);
        }
      };

      $elem.data(PLUGIN_NAME, {
        'methods': methods
      });
    });
  }
})(jQuery, window, document);
