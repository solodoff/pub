/**
 *  @author solodoff@yandex.ru
 *
 *  ToDo: поправить metrics-window
 *
 *  Следит за прокруткой и ресайзом окна.
 *  Триггерит событие на элементе <body>, куда передаёт все метрики.
 *
 *  Использование:
 *    $('body').on('go', function(e, res){
 *      // что-то делаем с res...
 *    });
 *
 *    var scrollObserver = new ScrollObserver( {options} | null );
 *
 *
 *
 *  Принудительно пересчитать (например, если перестроили структуру документа):
 *    scrollObserver.refresh();
 *    или так:
 *    $(window).trigger('refresh.scrollObserver')
 *
 *  Выключить:
 *    scrollObserver.off();
 *    или так:
 *    $('body').off('.scrollObserver');
 *
 *
 *  Зависимости:
 *    jquery
 *
 *
 *  События на элементе body:
 *    go.scrollObserver вторым аргументом передаётся объект с метриками (структуру см. в DEFAULTS, плюс event)
 *
 */

import $ from 'jquery';

function ScrollObserver(options){
  this.options = $.extend({}, ScrollObserver.prototype.DEFAULTS, options);
  this.$body = $('body');
  this.$window = $(window);

  this._init();
  if(this.options.isRefreshOnStart){
    this.refresh();
  }
}

ScrollObserver.prototype.DEFAULTS = {
  isRefreshOnStart: true,
  watchers: {   // какие метрики считать
    window: {
      width: true,
      height: true,
      scroll: {
        left: true,
        top: true
      }
    },
    document: {
      width: true,
      height: true
    }
  }
};

ScrollObserver.prototype._init = function(){
  var t = this;
  var w = t.options.watchers;

  var handler = function(event){
    var res = {
      event: event,
      window: {
        width: w.window.width ? $(window).width() : null,
        height: w.window.height ? $(window).height() : null,
        scroll: {
          left: w.window.scroll.left ? $(window).scrollLeft() : null,
          top: w.window.scroll.top ? $(window).scrollTop() : null
        }
      },
      document: {
        width: w.document.width ? $(document).width() : null,
        height: w.document.height ? $(document).height() : null
      }
    };

    t.$body.trigger('go', [res]);
  };

  t.$window.on('scroll.scrollObserver resize.scrollObserver refresh.scrollObserver', handler);
  t.$body.addClass('scrollObserver').on('touchmove.scrollObserver', handler);
};

ScrollObserver.prototype.refresh = function(){
  this.$window.trigger('refresh.scrollObserver');
};

ScrollObserver.prototype.off = function(){
  this.$window.off('.scrollObserver');
  this.$body.off('.scrollObserver').removeClass('scrollObserver');
};

export default ScrollObserver;
