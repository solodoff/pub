/**
 *  @author solodoff@yandex.ru
 *  upd 25.07.2013
 *  --
 *
 *  Для проекта newcbr
 *
 *  Управляет видимостью элементов в контейнере по заданному сценарию.
 *
 *  На входе: контейнер с набором элементов.
 *
 *  Определения:
 *    Элемент - любой DOM-элемент. Элементы м.б. вложены друг в друга.
 *    Кадр - состояние, описывающее какие из элементов показаны.
 *    Страница - набор определённых элементов.
 *
 *
 *  Зависимости:
 *    jquery
 *
 *    ToDo: сделать проще механизм обработки f1 и f2
 *
 *
 *  Использование:
 *    <div class="container1">
 *      <div class="elem1" >содержимое 1-го кадра, страница 1</div>
 *      <div class="elem2" >содержимое 2-го кадра, страница 1</div>
 *      <div class="elem3" >содержимое 3-го кадра, страница 2</div>
 *      <div class="elem4" >содержимое 4-го кадра, страница 3</div>
 *    </div>
 *
 *    var data1 = [ // массив кадров
 *    //[страница, 'селектор элементов, которые будут отображаться в этом кадре']
 *      [1, '.elem1'],
 *      [1, '.elem1, .elem2', f1, f2],
 *      [2, '.elem3'],
 *      [3, '.elem4']
 *    ];
 *
 *    где f1 и f2:
 *
 *    f1 = function(elements){ // эффекты входа в кадр
 *      $(elements)...//делаем что-то с набором элементов текущего кадра
 *    }
 *
 *    f2 = function(elements, finish){ // эффекты выхода из кадра
 *      $(elements)...//делаем что-то с набором элементов текущего кадра
 *      if(finish) finish(); // Обязательная строка! Сюда передаётся функция входа в следующий кадр.
 *
 *    }
 *
 *    Если f1 или f2 не задана, то применяется стандартный эффект(fadeIn или fadeOut).
 *
 *
 *    var widget = new FrameSwitcher('.container1', {options}, data1);
 *
 *    widget.go(1);
 *
 *
 *
 *  События на элементе container:
 *    page.frameSwitcher передаётся номер страницы
 *
 *
 */
function FrameSwitcher(container, options, data){
  this.options = $.extend({}, FrameSwitcher.prototype.DEFAULTS, options);
  this.$container = $(container);
  this.$elements = this.$container.find('.el');
  this.frames = data;

  this.current_page_num = undefined;
  this.current_frame_num = undefined;
  this.current_isHidden = true;

  this.init();
}

FrameSwitcher.prototype.DEFAULTS = {
  'durationAnimHide': 400,
  'durationAnimShow': 400,
  'startPage': undefined
};

FrameSwitcher.prototype.init = function(){
  var widget = this;
  $.map(this.frames, function(v, i){
    v[1] = widget.$elements.filter(v[1]);
  });
  if(typeof(this.options.startPage) != 'undefined'){
    this.current_isHidden = false;
    this.go(this.options.startPage);
  }
};

FrameSwitcher.prototype.hideCurrent = function(){
  this.$elements.stop().fadeOut(0);
  this.current_isHidden = true;
};

FrameSwitcher.prototype.go = function(frame_num){
  if((this.current_frame_num == frame_num) && !this.current_isHidden) return;

  var prev_frame_num = this.current_frame_num;
  this.current_frame_num = frame_num;

  var widget = this;
  var frame_cfg = this.frames[frame_num];
  if(frame_cfg === undefined) return;

  var default_exit = function(elements, finish){
    $(elements).stop(true, false).fadeOut(widget.options.durationAnimHide)
    .promise().done(function(){
      if(finish) finish();
    });
  };

  var default_enter = function(elements){
    $(elements).stop(true, false).fadeIn(widget.options.durationAnimShow);
  };

  // предыдущий кадр
  var frame_prev_cfg;
  if(prev_frame_num != undefined){
    frame_prev_cfg = this.frames[prev_frame_num];
  }else{
    frame_prev_cfg = [];
    frame_prev_cfg[1] = '';
    frame_prev_cfg[3] = function(elements, finish){
      if(finish) finish();
    };
  }

  frame_prev_cfg[3] = frame_prev_cfg[3] || default_exit;
  frame_cfg[2] = frame_cfg[2] || default_enter;

  this.$elements.stop(true, false)
  .not(frame_cfg[1]).not(frame_prev_cfg[1]).hide();
  frame_prev_cfg[3](frame_prev_cfg[1], function(){
    frame_cfg[2](frame_cfg[1]);
  });

  if((this.current_page_num != frame_cfg[0]) || this.current_isHidden){
    this.current_page_num = frame_cfg[0];
    this.$container.trigger('page.frameSwitcher', [frame_cfg[0]]);
  }
  this.current_isHidden = false;
};

FrameSwitcher.prototype.go_progress = function(progress){
  var frame_num = Math.floor(this.frames.length * progress / 100);
  if(frame_num == this.frames.length){
    frame_num = this.frames.length - 1;
  }
  this.go(frame_num);
}
