import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Main from './components/Main';
import Menu from './components/Menu';
import About from './components/About';
import TaskInfo from './components/TaskInfo';
import Page404 from './components/Page404';

import './assets/styles/app.less';



class App extends React.Component {
  render() {
    return (
      <div className="layout__wrapper">
        <Menu />
        <Switch>
          <Route exact path="/" component={Main}/>
          <Route exact path="/about" component={About}/>

          <Route exact path="/tasks/:id/" component={TaskInfo}/>
          <Redirect from="/tasks/:id/" to="/" />

          <Route component={Page404}/>

        </Switch>

      </div>
    )
  }
}

export default App;