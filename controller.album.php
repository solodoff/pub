<?php
/** ...
 */
class controller_album implements i_controller {

    /**
     * Создаём новый альбом и перенаправляем юзера на его редактирование
     */
    public function index(){
        ...
        $user_id = $user_id_reg;
        if($user_id===null) $user_id = $user_id_noname;

        ...


        try{
            $user = app::manager_DB()->getUser($user_id);
            $yyyymm = $user['yyyymm'];

            //если аккаунт не продлён - выходим
            ...
            // проверка на уже созданный пустой альбом
            ...
            ...

            // создаём эвент в базе...
            $event_id = app::manager_DB()->addEvent($user_id);

            // ...и в хранилище
            // создаём юзера в хранилище(если он ещё не создан)
            app::manager_storage()->createUser($yyyymm, $user_id);
            app::manager_storage()->createEvent($yyyymm, $user_id, $event_id);
            ...
            ...

            $templates =  app::manager_DB()->getAllTemplatesRand();

            // собираем альбом
            $data_album = array();
            foreach($templates as $v){
                ...

                array_push($data_album, array($v['id'], $data_slot));
            }

            $album_id = app::manager_DB()->addAlbum($event_id, $default_title, $data_album);

            // добавляем в список пустых(для блокировки создания новых)
            app::manager_DB()->updateUserEmptyEventAdd($user_id, $event_id);

            app::core()->redirect(CFG_SITE_ROOT.'album/show/id/'.$album_id.SEP);
        }
        catch(Exception $e){
            app::log($e, app::LOG_ERROR);
            exit;
        }
    }

    /**
     * Показываем альбом
     */
    public function show($args){
        ...
        ...

        if($album_id<0) app::core()->redirect();

        try{
            // получаем альбом. если альбом удалён то редирект на автора
            $album = app::manager_DB()->getAlbum($album_id);

            //if($album['is_edit'] && !$is_edit_mode) // альбом залочен для редактирования!
        }
        catch(Exception $e){
            app::core()->redirect($user_id_reg ? CFG_SITE_ROOT.'author/show/id/'.$user_id_reg: CFG_SITE_ROOT);
        }

        try{
            // получаем эвент
            $event = app::manager_DB()->getEvent($album['event_id']);
            ...
            // сохраняем пометку в сессии
            ...
            // формируем альбом
            ...

            // получаем бар
            $bar = $this->getBar($album_id);
            $data['bar'] = $bar;

            //продлён ли аккаунт
            ...


            $slides_per_star = app::cfg_SLIDES_PER_STAR();

            // пинаем view
            $view = app::view_album();
            $view->title = $album['title'];
            $view->styles = $styles;
            $view->data = ....
            ...
            $view->show();
        }
        catch(Exception $e){
            app::log($e, app::LOG_ERROR);
            app::core()->redirect();
        }
    }

    /**
     * Формируем альбом по его id
     * @param int $album_id
     * @param bool $show_empty_slides
     * @param bool $is_edit_mode true если открывает автор для редактирования
     * @return array
     */
    private function getAlbum($album_id, $show_empty_slides, $is_edit_mode=false){
        // получаем альбом
        $album = app::manager_DB()->getAlbum($album_id);
        ...

        $res['title'] = $album['title'];

        // получаем контент альбома
        $album_data = app::manager_DB()->getAlbumData($album_id);

        // получаем id нужных шаблонов
        $templates_id = array();
        foreach($album_data as $v) array_push($templates_id, $v[0]);

        // одним запросом получаем нужные шаблоны
        $templates = app::manager_DB()->getTemplates($templates_id);
        ...

        // получаем стили
        $res['styles'] ='';
        foreach($templates as $v) $res['styles'] .=$v['styles'];

        // формируем контент
        $res['slides'] = array();
        foreach($album_data as $v){
            $types = $tmpl[$v[0]]['types_SER'];
            $data = $v[1];
            //if(count($types)!=count($data)) throw new Exception();

            // формируем слайд
            $slide['class_name'] = $tmpl[$v[0]]['class_name'];
            $slide['data'] = array();

            $is_empty_slide = true; // флаг, если все слоты окажутся пустыми, то слайд пустой
            foreach($data as $k=>$vv){
                array_push($slide['data'], array('type'=>$vv[0], 'value'=>$vv[1], 'limit'=>$types[$k][1]));
                if(!empty($vv[1])) $is_empty_slide = false;
            }

            // заглавный слайд показываем всегда
            if($v[0]==0) $is_empty_slide = false;

            // аттачим слайд к контенту (если нужно пустые, то и их)
            if(!$is_empty_slide || $show_empty_slides) array_push($res['slides'], $slide);
        }

        return $res;
    }

    /**
     * Формируем бар
     * @param int $album_id
     * @return array
     */
    private function getBar($album_id){
        // получаем альбом
        $album = app::manager_DB()->getAlbum($album_id);

        $event_id = $album['event_id'];

        // выбираем фото по event_id
        $photos = app::manager_DB()->getPhotos($event_id);

        $res = array();
        foreach($photos as $photo){
            // используется ли уже фотка в альбоме
            $k = array_keys($photo['in_albums_SER'], $album_id);
            $is_used = !empty($k);

            //app::log('>>> $is_used='.$is_used);

            array_push($res, array($photo['id'], $is_used));
        }

        return $res;
    }

    /**
     * Обновляет заглавное фото альбома
     * @param int $album_id
     * @return bool
     */
    public function refreshMainPhoto($album_id){
        ...

        $main_photo_id = 0;

        try{
            $album = app::manager_DB()->getAlbum($album_id);
            $album_data = app::manager_DB()->getAlbumData($album_id);

            // получаем id нужных шаблонов
            $templates_id = array();
            foreach($album_data as $v) array_push($templates_id, $v[0]);

            // одним запросом получаем нужные шаблоны
            $templates = app::manager_DB()->getTemplates($templates_id);
            // ключи=id слайдов
            ...

            // пробегаемся по data в поисках первой фотки
            foreach($album_data as $v){
                $types = $tmpl[$v[0]]['types_SER'];
                $data = $v[1];
                //if(count($types)!=count($data)) throw new Exception();

                foreach($data as $k=>$vv){
                    if(($vv[0]==app::SLOT_PHOTO) && !empty($vv[1])){
                        $main_photo_id = $vv[1];
                        break(2);
                    }
                }
            }

            // если не нашли, то считываем и берём 1ю фотку эвента
            if($main_photo_id == 0){
                $photo = app::manager_DB()->getPhotoFirst($album['event_id']);
                if(count($photo)) $main_photo_id = $photo['id'];
            }

            // проверяем и если изменилось то апдейтим main_photo
            if($album['main_photo'] != $main_photo_id){
                app::manager_DB()->updateAlbumPhoto($album_id, $main_photo_id);
            }

            app::log('... УСПЕШНО refreshMainPhoto...$album_id='.$album_id.' $main_photo_id='.$main_photo_id, app::LOG_DEBUG);

            return true;
        }
        catch(Exception $e){
            app::log($e, app::LOG_ERROR);
            return false;
        }
    }

    ...


    ...

}
?>
