const stateKey = 'state';


export const loadState = () => {
  try{
    const serializedState = localStorage.getItem(stateKey);

    if(serializedState === null){
      return undefined;
    }

    return JSON.parse(serializedState);
  } catch(err){
    return undefined;
  }
};


export const saveState = (state) => {
  try{
    const serializedSate = JSON.stringify(state);
    localStorage.setItem(stateKey, serializedSate);
  } catch(err){
    //
  }
};
