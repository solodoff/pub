/**
 *  @author solodoff@yandex.ru
 *  upd 08.07.2013
 *  --
 *
 *  Виджет фиксирует элемент на странице на определённую часть прокрутки.
 *  Элементу передаётся прогресс прокрутки (можно использовать, например,
 *  для горизонтального скролла внутри элемента).
 *
 *  Использование:
 *    var widget1 = new MagicScroller(elem, {options});
 *
 *    ! - у elem д.б. жёстко задана высота
 *
 *
 *  Зависимости:
 *    jquery
 *
 *
 *  События на элементе elem:
 *    go.magicScroller передаётся progress
 *    start.magicScroller
 *    stop.magicScroller
 *
 *
 *  CSS:
 *    на elem по мере прокрутки страницы вешаются классы, обозначающие 3 режима:
 *    1) прокрутка перед элементом
 *    2) прокрутка, кода элемент на экране
 *    3) прокрутка после элемента
 *
 *    пример стилей для этих классов:
 *
 *    .MS_mode1{
 *      position: static;
 *      top: auto;
 *      bottom: auto;
 *      left: auto;
 *      right: auto;
 *    }
 *    .MS_mode2{
 *      position: fixed;
 *      top: 0;
 *      bottom: auto;
 *      left: 50%; margin-left: -500px;
 *      right: auto;
 *    }
 *    .MS_mode3{
 *      position: absolute;
 *      top: auto;
 *      bottom: 0;
 *      left: auto;
 *      right: auto;
 *    }
 *
 */
function MagicScroller(elem, options){
  this.options = $.extend({}, MagicScroller.prototype.DEFAULTS, options);
  this.$elem = $(elem);

  this.$outer = $('<div />').addClass('outer')
  .css({
    'position': 'relative'
  });
  this.$inner = $('<div />').addClass('inner');
  this.outer_height = undefined;

  this.mode = 1;
  this.point = {
    'start': undefined,
    'stop': undefined
  };

  this.init();
}

MagicScroller.prototype.DEFAULTS = {
  'scrollHeight': 3000,

  /**
   *  Когда считать позицию начала магического скролла: один раз при инициализации или постоянно.
   *  Постоянно пересчиывать нужно если положение контейнера относительно начала документа меняется.
   *  Например, при изменении размера окна, или скриптами динамически добавится/удалится элемент перед
   *  контейнером.
   */
  'isCalcPointsOnce': false
};

MagicScroller.prototype.progress = function(position){
  var progress = Math.round((position - this.point.start) / this.options.scrollHeight * 100);
  this.$elem.trigger('go.magicScroller', [progress]);
};

MagicScroller.prototype.init = function(){
  var widget = this;

  this.$elem.wrap(this.$outer).wrap(this.$inner);

  this.$inner = this.$elem.closest('.inner');
  this.$outer = this.$inner.closest('.outer');

  this.$outer.css('height', this.$elem.outerHeight(true) + this.options.scrollHeight + 'px');

  if(this.options.isCalcPointsOnce) this.calcPoints();

  $(window)
  .on('scroll.magicScroller resize.magicScroller', function(){
    var position = $(document).scrollTop();
    widget.checkMode(position);
    if(widget.mode == 2) widget.progress(position);
  })
  .trigger('scroll'); // mode init
};

MagicScroller.prototype.checkMode = function(position){
  if(!this.options.isCalcPointsOnce) this.calcPoints();

  if(position < this.point.start){
    this.setMode(1);
  }else if(position > this.point.stop){
    this.setMode(3);
  }else{
    this.setMode(2);
  }
};

MagicScroller.prototype.calcPoints = function(){
  this.point.start = this.$outer.offset().top;
  this.point.stop = this.point.start + this.options.scrollHeight;
};

MagicScroller.prototype.setMode = function(mode){
  if(this.mode == mode) return;
  this.mode = mode;

  switch(mode){
    case 1:
      this.$inner.removeClass('MS_mode2 MS_mode3').addClass('MS_mode1');
      this.$elem.trigger('stop.magicScroller');
      break;

    case 2:
      this.$inner.removeClass('MS_mode1 MS_mode3').addClass('MS_mode2');
      this.$elem.trigger('start.magicScroller');
      break;

    case 3:
      this.$inner.removeClass('MS_mode1 MS_mode2').addClass('MS_mode3');
      this.$elem.trigger('stop.magicScroller');
      break;
  }
};
