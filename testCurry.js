

function curry(func) {
  let args = [];
  const argsRequired = func.length;

  return function f() {
    let result;
    args = args.concat(Array.prototype.slice.call(arguments));

    if(args.length < argsRequired){
      result = f;
    } else {
      result = func.apply(null, args);
      args.length = 0;
    }

    return result;
  };
};


function sumThree(a, b, c) {
  return a + b + c;
};

const sum = curry(sumThree);

// function sum(a){
//   return function (b) {
//     return a + b;
//   };
// };


console.log('sum(14, 2)(6)=', sum(14, 2)(6));
console.log('sum(13)()(-10)()(1)=', sum(13)()(-10)()(1));
